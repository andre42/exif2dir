# exif2dir

Erzeugt aus Exifinformationen in JPEGs eine Directorystruktur
Thu Jan 24 21:27:59 CET 2019

Ausführen z.B. mit

mvn -q --offline compile

mvn -q --offline exec:java -Dexec.args="--source src/test/resources/examples --destination destinationdir"






Offene Dinge:

ED000. OK, GITLab aufsetzen

ED001. OK, Commandlineparser einbauen

ED002. OK, Programmstruktur erstellen

ED003. OK, Lib zum Exifextrahieren finden

ED004. OK: AW: Datum/Zeitinformation aus Exifdaten extrahieren und verarbeitbar machen.

ED005. In Arbeit: Directorystruktur aus extrahierter Information erstellen


