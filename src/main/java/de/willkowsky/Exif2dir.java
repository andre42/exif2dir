package de.willkowsky;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static java.text.MessageFormat.format;

class Exif2dir {
    static private boolean verbose;
    private String sourceDirectory;
    protected String destinationDirectory;
    private FilesystemHelper filesystemHelper;
    
    Exif2dir(FilesystemHelper filesystemHelper) {
        this.filesystemHelper = filesystemHelper;
    }
    
    /**
     * Damit keine System.out.printlns im Code verteilt werden
     */
    static void out(String message) {
        if (verbose) {
            System.out.println(message);
        }
    }
    
    /**
     * Das Datum im Datestring kann als 2019-01-01 23:56:57
     * geschrieben worden sein oder als 2019:01:01 23:56:57. Deshalb
     * wird ":" und "-" durch "/" ersetzt.
     * Das Tagesdatum wird durch split(" ") von der Uhrzeit getrennt.
     */
    private String dateToDir(String dateString) {
        if (dateString.contains(" ")) {
            String result = dateString.split(" ")[0].replace(":", "/")
                    .replace("-", "/");
            
            out(format("dateToDir, vorher: {0} nachher: {1}", dateString,
                    result));
            
            return result;
        }
        
        return null;
    }
    
    File[] getAllFilesFromDirectory(String arg) {
        File[] result = null;
        
        if (arg != null) {
            File file = new File(arg);
            result = file.listFiles();
        }
        
        return result;
    }
    
    /**
     * Erzeugt eine Map, in der die Dateinamen der Bilder zu ihren, aus den
     * Exif-Informationen gelesenen und generierten Verzeichnissen,
     * zugeordnet werden.
     *
     * @param filenameDateMap Map, in der zu einem Filenamen eine
     *                        Stringrepresentation eines Datums zugeordnet ist.
     * @return
     */
    Map<String, String> buildFilenameToTargetDirMap(Map<String, String> filenameDateMap) {
        Map<String, String> result = new HashMap<>();
        
        filenameDateMap.forEach((filenameWithFullPath, dateString) -> {
            out(format("filenameWithFullPath is {0}: datestring is {1}",
                    filenameWithFullPath,
                    dateString));
            
            String dateToDir = dateToDir(dateString);
            if (dateToDir != null) {
                Path fileNameWithoutPath = Paths.get(filenameWithFullPath)
                        .getFileName();
                String destinationFilename = format("{0}/{1}/{2}",
                        destinationDirectory, dateToDir, fileNameWithoutPath);
                out("Targetfilename is " + destinationFilename);
                result.put(filenameWithFullPath, destinationFilename);
            }
        });
        
        return result;
    }
    
    /**
     * Erzeugt eine Map in der ein Filename eines Bildes zu einem Datum aus
     * den Metainformationen des JPGs zugeordnet wird.
     *
     * @param args Die Standardkommandozeilenargumente.
     */
    Map<String, String> buildFilenameToDateMap(String[] args) {
        
        Map<String, String> result = new HashMap<>();
        
        if (!parseCmdlineArguments(args)) {
            // Wenn das Parsen der Argumente false liefert, dann sollen keine
            // weiteren Aktionen erfolgen.
            return result;
        }
        
        File[] allFilesFromDirectory = getAllFilesFromDirectory(
                sourceDirectory);
        if (allFilesFromDirectory != null) {
            for (File file : allFilesFromDirectory) {
                result.putAll(readAndDisplayMetadata(file));
            }
        }
        
        return result;
    }
    
    /**
     * Liefert true, wenn die Kommanndozeile korrekte Argumente enthaelt.
     *
     * @param args Die Kommandozeilenargumente.
     * @return true, wenn kein Fehler beim Parsen der Kommandozeile
     * aufgetreten ist und nicht nur die Hilfe angezeigt werden soll.
     */
    private boolean parseCmdlineArguments(String[] args) {
        ProgramArgumentHandler programArgumentHandler = new ProgramArgumentHandler();
        boolean programArgumentsValid = programArgumentHandler
                .processProgramArguments(args);
        
        if (programArgumentsValid) {
            this.sourceDirectory = programArgumentHandler.getSourceDirectory();
            this.destinationDirectory =
                    programArgumentHandler.getDestinationDirectory();
            verbose = programArgumentHandler.isVerboseModeSpecified();
        }
        
        return programArgumentsValid;
    }
    
    // TODO: aw, diese Methode ist zu lang, bitte aufteilen
    Map<String, String> readAndDisplayMetadata(File file) {
        Map<String, String> pictureDateMap = new HashMap<>();
        Metadata metadata = null;
        
        try {
            metadata = ImageMetadataReader.readMetadata(file);
        } catch (Exception e) {
            out("cannot read metadata from filename: " +
                    file.getAbsolutePath());
            out(e.toString());
        }
        
        if (metadata != null) {
            String dateFromMetadata = null;
            for (Directory metadataDirectory : metadata.getDirectories()) {
                if (handlePossibleMetadataErrors(metadataDirectory)) {
                    break;
                }
                
                dateFromMetadata = getDateFromMetadata("Date/Time Digitized",
                        metadataDirectory);
                
                if (dateFromMetadata != null) {
                    pictureDateMap
                            .put(file.getAbsolutePath(), dateFromMetadata);
                    break;
                }
            }
            
            if (dateFromMetadata == null) {
                out("no digitizer tag found for file " +
                        file.getAbsolutePath());
            }
        }
        
        return pictureDateMap;
    }
    
    private boolean handlePossibleMetadataErrors(Directory metadataDirectory) {
        if (metadataDirectory.hasErrors()) {
            for (String error : metadataDirectory.getErrors()) {
                out("error: " + error);
            }
            return true;
        }
        return false;
    }
    
    /**
     * Liefert den Datumstring aus dem Metadatendirectory.
     *
     * @param metadataDirectory Das Directory mit allen Metadaten.
     * @return Das Datum, welches zum ersten Metadaten-Tag im JPG hinterlegt
     * ist, das den übergebenen Substring im Tagnamen hat.
     */
    private String getDateFromMetadata(String tagNameSubstring,
                                       Directory metadataDirectory) {
        String dateFromMetadata = null;
        for (Tag tag : metadataDirectory.getTags()) {
            if (tag.getTagName().contains(tagNameSubstring)) {
                out(String.format("%s = %s\n", tag.getTagName(),
                        tag.getDescription()));
                dateFromMetadata = tag.getDescription();
                break;
            }
        }
        
        return dateFromMetadata;
    }
    
    void moveFilenameToTargetDir(Map<String, String> filenameToTargetDirMap) {
        filenameToTargetDirMap.forEach(
                (sourceFilename, targetFilename) -> filesystemHelper
                        .moveFile(sourceFilename, targetFilename));
    }
}
