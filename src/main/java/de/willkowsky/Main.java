package de.willkowsky;

import java.util.Map;

import static de.willkowsky.Exif2dir.out;

public class Main {
    public static void main(String[] args) {
        Exif2dir exif2dir = new Exif2dir(new FilesystemHelper());
        Map<String, String> filenameToDateStringMap = exif2dir
                .buildFilenameToDateMap(args);
        
        out("Anzahl an Bildern die verschoben werden: " +
                filenameToDateStringMap.entrySet().size());
        
        if (filenameToDateStringMap.isEmpty()) {
            return;
        }
        
        Map<String, String> filenameToTargetDirMap = exif2dir
                .buildFilenameToTargetDirMap(filenameToDateStringMap);
        
        exif2dir.moveFilenameToTargetDir(filenameToTargetDirMap);
    }
}
