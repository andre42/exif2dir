package de.willkowsky;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.MessageFormat;

import static de.willkowsky.Exif2dir.out;
import static java.text.MessageFormat.format;

/**
 * Alle Aktionen, die auf dem Filesystem durchgeführt werden, sollen in dieser
 * Klasse stattfinden.
 */
public class FilesystemHelper {
    private boolean dryrun = false;
    
    public void moveFile(String from, String to) {
        try {
            out(format("creating directory {0}", Paths.get(to).getParent()));
            if (!dryrun) {
                Files.createDirectories(Paths.get(to).getParent());
            }
            out(format("moving file {0} to {1}", from, to));
            if (!dryrun) {
                Files.move(Paths.get(from), Paths.get(to));
            }
        } catch (IOException e) {
            out(format("Error when creating directories or moving file: {0}",
                            e.toString()));
        }
    }
}
