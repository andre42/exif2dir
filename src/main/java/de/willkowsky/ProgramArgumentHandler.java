package de.willkowsky;

import org.apache.commons.cli.*;

import static de.willkowsky.Exif2dir.out;

class ProgramArgumentHandler {
    
    private static final String SOURCE = "source";
    private static final String VERBOSE = "verbose";
    private static final String DRYRUN = "dryrun";
    private static final String HELP = "help";
    private static final String DESTINATION = "destination";
    private CommandLine commandLine;
    
    /**
     * Liefert false, wenn ein Problem beim Parsen der Kommandozeile
     * aufgetreten ist oder nur die Hilfe angezeigt werden soll.
     * Liefert true, wenn keine Fehler festgestellt wurden und die eigentliche
     * Funktion weiter durchgeführt werden soll.
     */
    boolean processProgramArguments(String[] args) {
        Options options = buildOptions();
        initCommandline(args, options);
        String additionalHelpInfo = "";
        
        boolean commandLineValid = false;
        if (commandLine != null) {
            // wurde das source- und Destinationdirectory spezifiziert?
            commandLineValid =
                    isSourceDirSpecified() && isDestinationDirSpecified();
            
            if (!isSourceDirSpecified()) {
                additionalHelpInfo += "Missing source directory " +
                        "specification";
            }
            
            if (!isDestinationDirSpecified()) {
                if (!additionalHelpInfo.isEmpty()) {
                    additionalHelpInfo += ", ";
                }
                
                additionalHelpInfo += "missing destination directory " +
                        "specification";
            }
        }
        
        if (!commandLineValid) {
            out("\n" + additionalHelpInfo);
            new HelpFormatter()
                    .printHelp("exif2dir {-s DIR} {-d DIR}", options);
        }
        
        return commandLineValid;
    }
    
    private void initCommandline(String[] args, Options options) {
        try {
            commandLine = new DefaultParser().parse(options, args);
        } catch (ParseException parseException) {
            out("exif2dir: " + parseException.getMessage());
        }
    }
    
    private boolean isDestinationDirSpecified() {
        String optionValue = commandLine.getOptionValue(DESTINATION);
        if (optionValue == null || "".equals(optionValue)) {
            
            return false;
        }
        
        return true;
        
    }
    
    private boolean isSourceDirSpecified() {
        String optionValue = commandLine.getOptionValue(SOURCE);
        if (optionValue == null || "".equals(optionValue)) {
            
            return false;
        }
        
        return true;
    }
    
    private Options buildOptions() {
        Options options = new Options();
        options.addOption("s", SOURCE, true, "Source folder with images to " +
                "move.");
        options.addOption("d", DESTINATION, true, "Destination directory. The" +
                " images will be moved here.");
        options.addOption("v", VERBOSE, false, "Be more verbose. Show whats " +
                "being done.");
        options.addOption("n", DRYRUN, false, "Do not apply any changes.");
        options.addOption("h", HELP, false, "Display this help message.");
        
        return options;
    }
    
    String getSourceDirectory() {
        if (commandLine != null) {
            return commandLine.getOptionValue(SOURCE);
        } else {
            return null;
        }
    }
    
    String getDestinationDirectory() {
        if (commandLine != null) {
            return commandLine.getOptionValue(DESTINATION);
        } else {
            return null;
        }
    }
    
    public boolean isVerboseModeSpecified() {
        String optionValue = commandLine.getOptionValue(VERBOSE);
        if (optionValue == null || "".equals(optionValue)) {
            return false;
        }
        return true;
    }
}
