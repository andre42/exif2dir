package de.willkowsky;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Spy;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * In dieser Klasse werden die Operationen auf dem Filesystem durch Methoden
 * ersetzt, die die Aufrufe zählen.
 */
class FilesystemHelperTestClass extends FilesystemHelper {
    @Override
    public void moveFile(String from, String to) {
    }
}

class Exif2dirTest {
    
    @Spy
    FilesystemHelperTestClass filesystemHelperTestClass;
    
    private Exif2dir exif2dir = null;
    
    @SuppressWarnings("unused")
    @BeforeEach
    private void init() {
        initMocks(this);
        if (exif2dir == null) {
            exif2dir = new Exif2dir(filesystemHelperTestClass);
            exif2dir.destinationDirectory = "mydestinationdir";
        }
    }
    
    @Test
    @DisplayName("Displayname of Simpletestexample")
    void testApp() {
        URL resource = this.getClass().getClassLoader()
                .getResource("./examples");
        
        if (resource == null) {
            fail();
        }
        
        File[] files = exif2dir.getAllFilesFromDirectory(resource.getPath());
        for (File file : files) {
            System.out.println(file.getName());
        }
    }
    
    @Test
    void testDirectoryCreation() {
        Map<String, String> testFilenameToDateMap = new HashMap<>();
        String DIRS1 = "/dir1/dir2/dir3/bild1.jpg";
        String DIRS2 = "/dir4/dir5/dir6/bild2.JPG";
        String DIRS3 = "/dir5/dir5/dir6/bild2.JPG";
        
        testFilenameToDateMap.put(DIRS1, "2019-10-01 12:34:56");
        testFilenameToDateMap.put(DIRS2, "1978-02-31 25:61:70");
        testFilenameToDateMap.put(DIRS3, "1978-02-3125:61:70");
        
        Map<String, String> filenameToTargetDirMap = exif2dir
                .buildFilenameToTargetDirMap(testFilenameToDateMap);
        
        assertEquals("mydestinationdir/2019/10/01/bild1.jpg",
                filenameToTargetDirMap.get(DIRS1));
        assertEquals("mydestinationdir/1978/02/31/bild2.JPG",
                filenameToTargetDirMap.get(DIRS2));
    }
    
    @Test
    void testMovePictures() {
        Map<String, String> testFilenameToDateMap = new HashMap<>();
        String DIRS1 = "/dir1/dir2/dir3/bild1.jpg";
        String DIRS2 = "/dir4/dir5/dir6/bild2.JPG";
        String DIRS3 = "/dir5/dir5/dir6/bild2.JPG";
        
        testFilenameToDateMap.put(DIRS1, "2019/10/01");
        testFilenameToDateMap.put(DIRS2, "1978/02/31");
        testFilenameToDateMap.put(DIRS3, "1978/02/31");
        
        exif2dir.moveFilenameToTargetDir(testFilenameToDateMap);
        
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor
                .forClass(String.class);
        verify(filesystemHelperTestClass, times(3)).moveFile(
                argumentCaptor.capture(), argumentCaptor.capture());
        assertEquals("/dir1/dir2/dir3/bild1.jpg",
                argumentCaptor.getAllValues().get(0));
        assertEquals("2019/10/01", argumentCaptor.getAllValues().get(1));
        assertEquals("/dir4/dir5/dir6/bild2.JPG",
                argumentCaptor.getAllValues().get(2));
        assertEquals("1978/02/31", argumentCaptor.getAllValues().get(3));
        assertEquals("/dir5/dir5/dir6/bild2.JPG",
                argumentCaptor.getAllValues().get(4));
        assertEquals("1978/02/31", argumentCaptor.getAllValues().get(5));
    }
    
    @Test
    void testBuildFilenameToDateMap() {
        
        Exif2dir exif2dirMock = mock(Exif2dir.class);
        File file1 = mock(File.class);
        File file2 = mock(File.class);
        File[] fileArray = {file1, file2};
        
        String[] commandlineArgs = {"--source", "bla", "--destination",
                "blubb"};
        
        Map<String, String> stringstringmap = new HashMap<>();
        
        when(exif2dirMock.getAllFilesFromDirectory(any(String.class)))
                .thenReturn(fileArray);
        when(exif2dirMock.readAndDisplayMetadata(any(File.class)))
                .thenReturn(stringstringmap);
        when(exif2dirMock.buildFilenameToDateMap(any(String[].class)))
                .thenCallRealMethod();
        
        exif2dirMock.buildFilenameToDateMap(commandlineArgs);
        
        verify(exif2dirMock, times(2)).readAndDisplayMetadata(any(File.class));
        
    }
    
}

